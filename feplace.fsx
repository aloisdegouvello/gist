let filePath = """C:\Users\adegouvello\Desktop\filePath.txt"""
let linePath = """C:\Users\adegouvello\Desktop\linePath.txt"""

let filePaths = System.IO.File.ReadLines filePath
let linePaths = System.IO.File.ReadLines linePath

// let filePaths = """
// C:\Migration\alphabet.txt(2)
// C:\Migration\number.txt(3)
// """

let getRowNumber line =
    line
    |> Seq.rev
    |> Seq.tail
    |> Seq.takeWhile System.Char.IsDigit
    |> Seq.rev
    |> System.String.Concat

let splitLineAndRowNumber (line: string): string * int =
    let rowNumber = getRowNumber line
    let limit = line.Length - (1 + 2 + rowNumber.Length)
    line.[0..limit], (rowNumber |> int) - 1

// let splitlines (x: string) =
//     x.Split ([| "\n" |], System.StringSplitOptions.RemoveEmptyEntries)

let replaceLine (newLine: string) (path: string, lineNumber: int) =
    let lines =
        System.IO.File.ReadAllLines path
        |> Seq.mapi (fun i line -> if i = lineNumber then newLine else line)
        |> Seq.toArray
    System.IO.File.WriteAllLines (path, lines)

replaceLine "toto" (@"C:\Migration\tmp\number.txt", 10)
replaceLine "toto" (@"C:\Migration\tmp\alphabet.txt", 1)

filePaths
|> Seq.map splitLineAndRowNumber
|> Seq.map (fun t -> replaceLine "test" t)